# release-watcher-helm-chart

this is a helm chart for `https://github.com/rycus86/release-watcher`

````
git clone https://gitlab.com/fehrlich/release-watcher-helm-chart.git
cd helm
```

create file `answers.yaml` like:

```
config:
  slackurl: your-slack-webhook-url
  default: |-
    releases:
      github:
        - owner:  docker
          repo:  docker-ce
          filter: '[0-9]+\.[0-9]+\.[0-9]+$'
```

For the format of the config see: https://github.com/rycus86/release-watcher#configuration

install the chart: `helm install -f answers.yml --namespace=release-watcher --name=release-watcher .`


## chart options:

|             Parameter                     |                     Description                     |                              Default                              |
|-------------------------------------------|-----------------------------------------------------|-------------------------------------------------------------------|
| `config.slackurl`                         | Slack Webhook url (required)                        | `nil`                                                             |
| `config.default`                          | Default Config                                      | an example config that watches docker                             |
| `image.url`                               | docker image url                                    | `rycus86/release-watcher:latest`                                  |
| `image.pullPolicy`                        | pull policy                                         | `IfNotPresent`                                                    |
| `persistance.enabled`                     | enable a persitance Volume for kubernetes           | `false`                                                           |
| `persistance.size`                        | Volume size                                         | `100M`                                                            |
| `persistance.storageClass`                | Kubernetes storage class                            | `nil`                                                             |
| `persistance.accessMode`                  | volume accessMode                                   | ReadWriteOnce                                                     |