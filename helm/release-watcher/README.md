# release-watcher

this is a helm chart for `https://github.com/rycus86/release-watcher`

## chart options:

|             Parameter                     |                     Description                     |                              Default                              |
|-------------------------------------------|-----------------------------------------------------|-------------------------------------------------------------------|
| `config.slackurl`                         | Slack Webhook url (required)                        | `nil`                                                             |
| `config.default`                          | Default Config                                      | an example config that watches docker                             |
| `image.url`                               | docker image url                                    | `rycus86/release-watcher:latest`                                  |
| `image.pullPolicy`                        | pull policy                                         | `IfNotPresent`                                                    |
| `persistance.enabled`                     | enable a persitance Volume for kubernetes           | `false`                                                           |
| `persistance.size`                        | Volume size                                         | `100M`                                                            |
| `persistance.storageClass`                | Kubernetes storage class                            | `nil`                                                             |
| `persistance.accessMode`                  | volume accessMode                                   | ReadWriteOnce                                                     |